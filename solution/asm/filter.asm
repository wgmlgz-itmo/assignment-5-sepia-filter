global sepia_simd

%define WIDTH_OFFSET 0
%define HEIGHT_OFFSET 8
%define DATA_OFFSET 16
%define PIXEL_LEN 3

%macro process_channel 2
  movdqu xmm3, xmm4        ; Copy pixel data to xmm3
  mulps xmm3, %1           ; Multiply by channel coefficient
  haddps xmm3, xmm3        ; Horizontal add
  haddps xmm3, xmm3        ; Horizontal add again
  cvtps2dq xmm3, xmm3      ; Convert to packed 32-bit integers
  extractps rax, xmm3, 0   ; Extract the first 32 bits
  cmp rax, 0xff         ; Compare rax with 0xff
  cmovg rax, rcx        ; Conditionally move rcx to rax if rax > 0xff
  mov byte[rdi + %2], al
%endmacro

section .data
r_mul: dd 0.189, 0.769, 0.393, 0.0
g_mul: dd 0.168, 0.686, 0.349, 0.0
b_mul: dd 0.131, 0.534, 0.272, 0.0

section .text
sepia_simd:
  mov rcx, 0xff                   ; Move 0xff to rcx for capping
  mov rdx, [rdi + WIDTH_OFFSET]   ; Load width
  imul rdx, [rdi + HEIGHT_OFFSET] ; Multiply width by height
  imul rdx, PIXEL_LEN             ; Multiply width by `sizeof(struct pixel)`
  mov rdi, [rdi + DATA_OFFSET]    ; Load ptr
  add rdx, rdi

  movdqu xmm0, [rel b_mul]
  movdqu xmm1, [rel g_mul]
  movdqu xmm2, [rel r_mul]

.loop:
  movdqu xmm4, [rdi] ; Load pixel data into xmm4
  pmovzxbd xmm4, xmm4 ; Convert byte data to dword
  cvtdq2ps xmm4, xmm4 ; Convert dword to floating point

  process_channel xmm0, 0
  process_channel xmm1, 1
  process_channel xmm2, 2

  add rdi, PIXEL_LEN
  cmp rdi, rdx
  jl .loop
  ret