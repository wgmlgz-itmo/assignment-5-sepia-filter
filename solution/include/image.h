#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdint.h>

struct pixel {
  uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

void sepia_simd(struct image const *source_image);

struct image sepia(struct image const source);

#endif  // IMAGE_H
