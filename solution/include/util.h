#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/bmp.h"
#include "../include/image.h"

void apply_sepia(char* source_image_path, char* transformed_image_path);
