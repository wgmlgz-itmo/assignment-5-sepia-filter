#include "../include/bmp.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/image.h"

struct __attribute__((packed)) bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

enum read_status from_bmp(FILE* in, struct image* img) {
  struct bmp_header header;

  if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
    return READ_INVALID_HEADER;
  }

  if (header.bfType != 0x4D42) {
    return READ_INVALID_SIGNATURE;
  }

  if (header.biBitCount != 24) {
    return READ_INVALID_BITS;
  }

  img->width = header.biWidth;
  img->height = header.biHeight;

  img->data = malloc(sizeof(struct pixel) * img->width * img->height);
  if (!img->data) {
    return READ_INVALID_HEADER;
  }

  uint8_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;

  for (uint64_t y = 0; y < img->height; y++) {
    for (uint64_t x = 0; x < img->width; x++) {
      if (fread(&img->data[y * img->width + x], sizeof(struct pixel), 1, in) !=
          1) {
        free(img->data);
        return READ_INVALID_HEADER;
      }
    }
    fseek(in, padding, SEEK_CUR);
  }

  return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
  struct bmp_header header = {
      .bfType = 0x4D42,
      .bfileSize = sizeof(struct bmp_header) +
                   img->width * img->height * sizeof(struct pixel),
      .bfReserved = 0,
      .bOffBits = sizeof(struct bmp_header),
      .biSize = 40,
      .biWidth = img->width,
      .biHeight = img->height,
      .biPlanes = 1,
      .biBitCount = 24,
      .biCompression = 0,
      .biSizeImage = img->width * img->height * sizeof(struct pixel),
      .biXPelsPerMeter = 2835,
      .biYPelsPerMeter = 2835,
      .biClrUsed = 0,
      .biClrImportant = 0};

  if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
    return WRITE_ERROR;
  }

  uint8_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;

  for (uint64_t y = 0; y < img->height; y++) {
    for (uint64_t x = 0; x < img->width; x++) {
      if (fwrite(&img->data[y * img->width + x], sizeof(struct pixel), 1,
                 out) != 1) {
        return WRITE_ERROR;
      }
    }
    for (int p = 0; p < padding; p++) {
      fputc(0, out);
    }
  }

  return WRITE_OK;
}
