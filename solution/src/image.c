
#include "../include/image.h"

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image sepia(struct image const source) {
  struct image processed;

  processed.width = source.width;
  processed.height = source.height;
  processed.data = (struct pixel*)malloc(sizeof(struct pixel) *
                                         processed.width * processed.height);

  if (!processed.data) {
    return processed;  // handled by caller
  }

  for (size_t i = 0; i < processed.width * processed.height; ++i) {
    processed.data[i] = source.data[i];
  }

#ifdef USE_SIMD
  sepia_simd(&processed);
  return processed;
#else

  for (uint64_t i = 0; i < source.width * source.height; i++) {
    struct pixel src_pixel = source.data[i];
    struct pixel* res_pixel = processed.data + i;

    float r = src_pixel.r;
    float g = src_pixel.g;
    float b = src_pixel.b;

    float out_r = (r * .393f) + (g * .769f) + (b * .189f);
    float out_g = (r * .349f) + (g * .686f) + (b * .168f);
    float out_b = (r * .272f) + (g * .534f) + (b * .131f);

    res_pixel->r = out_r > 0xFF ? 0xFF : (uint8_t)ceilf(out_r);
    res_pixel->g = out_g > 0xFF ? 0xFF : (uint8_t)ceilf(out_g);
    res_pixel->b = out_b > 0xFF ? 0xFF : (uint8_t)ceilf(out_b);
  }

  return processed;
#endif
}
