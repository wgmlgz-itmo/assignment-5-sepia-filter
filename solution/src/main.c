#include <stdio.h>
#include <stdlib.h>

#include "../include/util.h"

void process_arguments(int argc, char *argv[], char **source_image_path,
                       char **transformed_image_path) {
  if (argc != 3) {
    fprintf(stderr,
            "Usage: ./image-transformer <source-image> <transformed-image> "
            "\n");
    exit(1);
  }

  *source_image_path = argv[1];
  *transformed_image_path = argv[2];
}

int main(int argc, char *argv[]) {
  char *source_image_path;
  char *transformed_image_path;

  process_arguments(argc, argv, &source_image_path, &transformed_image_path);
  apply_sepia(source_image_path, transformed_image_path);
}
