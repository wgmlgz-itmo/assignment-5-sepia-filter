#!/bin/bash

CMAKE_CTEST_ARGUMENTS=$@

# Function to run tests and capture execution time
run_tests() {
    test_pattern=$1
    total_time=0
    for i in {1..10}; do
        output=$(ctest -R "$test_pattern" $CMAKE_CTEST_ARGUMENTS)
        time=$(echo "$output" | grep "Total Test time (real)" | awk '{print $6}')
        time_ms=$(echo $time | awk -F. '{ printf("%d\n", $1 * 1000 + $2); }')
        total_time=$((total_time + time_ms))
    done
    echo $total_time
}

# Function to convert seconds to milliseconds
convert_to_milliseconds() {
    echo $1 | awk -F. '{ printf("%d\n", $1 * 1000 + $2); }'
}

# Run C version tests and capture time
c_time=$(run_tests "^test-.*-c$")
echo "C version time: $c_time ms"

# Run SIMD version tests and capture time
simd_time=$(run_tests "^test-.*-simd$")
echo "SIMD version time: $simd_time ms"

# Compare times and exit with an error if SIMD is slower
if [ "$simd_time" -gt "$c_time" ]; then
    echo "Failure: SIMD version is slower than C version."
    echo "SIMD: $simd_time ms, C: $c_time ms"
    exit 1
else
    echo "Success: SIMD version is faster or equal to C version."
    echo "SIMD: $simd_time ms, C: $c_time ms"
fi
